import React, { useCallback, useContext } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { UserMenu } from './components';
import { useHistory, Link } from 'react-router-dom';
import { AuthContext } from 'services/AuthContext';

export function TopBar() {
    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            root: {
                flexGrow: 1,
            },
            menuButton: {
                marginRight: theme.spacing(2),
            },
            title: {
                flexGrow: 1,
            },
            link: {
                color: 'inherit',
                textDecoration: 'none',
            },
        }),
    );

    const classes = useStyles();
    const history = useHistory();
    const { user } = useContext(AuthContext);

    const handleMenuClick = useCallback(() => {
        console.log('click!');
    }, []);

    const handleLoginButtonClick = useCallback(() => {
        history.push('/login');
    }, [history]);

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="menu"
                        onClick={handleMenuClick}
                    >
                        <MenuIcon />
                    </IconButton>

                    <Typography variant="h6" className={classes.title}>
                        <Link className={classes.link} to="/">
                            ReactJS tutorial shop
                        </Link>
                    </Typography>

                    {user === null && (
                        <Button color="inherit" onClick={handleLoginButtonClick}>
                            Login
                        </Button>
                    )}
                    {user !== null && <UserMenu handleClick={handleMenuClick}></UserMenu>}
                </Toolbar>
            </AppBar>
        </div>
    );
}
