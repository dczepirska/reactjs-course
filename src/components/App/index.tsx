import React from 'react';
import './App.css';
import { TopBar } from './components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Dashboard, LoginPage, ProductsPage } from 'pages';
import { makeStyles, Container, CssBaseline } from '@material-ui/core';
import 'services/firebase';
import { AuthContextProvider } from 'services/AuthContext';

export function App() {
    const classes = useStyles();

    return (
        <AuthContextProvider>
            <Router>
                <TopBar />
                <Container maxWidth="lg" component="main" className={classes.main}>
                    <CssBaseline />
                    <Switch>
                        <Route exact path="/">
                            <ProductsPage />
                        </Route>
                        <Route exact path="/login">
                            <LoginPage />
                        </Route>
                        <Route exact path="/products">
                            <ProductsPage />
                        </Route>
                    </Switch>
                </Container>

                <footer>footer</footer>
            </Router>
        </AuthContextProvider>
    );
}

const useStyles = makeStyles((theme) => ({
    main: {
        flexGrow: 1,
        flexShrink: 0,
    },
}));
