import React, { useState, useEffect, useCallback } from 'react';
import { Product, getProducts } from 'services/products';
import { ProductCard } from './components';

export function ProductsPage() {
    const [products, setProducts] = useState<Product[] | null>(null);

    const [totalCount, setTotalCount] = useState(0);

    useEffect(() => {
        setTimeout(() => {
            getProducts(0, 10).then(({ list, totalCount }) => {
                setProducts(list);
                setTotalCount(totalCount);
            });
        }, 500);
    }, []);

    const addToCart = useCallback((productId: string) => {
        console.log(`Item ${productId} added to cart`);
    }, []);

    return (
        <div>
            <h2>products</h2>
            {products === null && <p>Loading...</p>}
            {products !== null && products.length === 0 && <p>Empty products list</p>}
            {products !== null &&
                products.length > 0 &&
                products.map((p) => <ProductCard {...p} addToCart={addToCart} key={p.id} />)}
        </div>
    );
}
