import React, { useCallback, MouseEvent } from 'react';
import { Product } from 'services/products';
import { Card, Button, Avatar, CardContent, Typography, CardActions, makeStyles } from '@material-ui/core';
import { Label } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

interface ProductCardProps extends Product {
    addToCart(id: string): void;
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export function ProductCard({
    addToCart,
    id,
    shortDescription,
    thumbnail,
    price,
    name,
    description,
}: ProductCardProps) {
    const classes = useStyles();
    const history = useHistory();

    const handleAddToCart = useCallback(
        (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            addToCart(id);
        },
        [id],
    );

    const handleGoToProduct = useCallback(
        (e: MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            history.push(`product/${id}`);
        },
        [id, history],
    );

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Avatar alt={shortDescription} src={thumbnail}>
                    <Label />
                </Avatar>
                <Typography gutterBottom variant="h5" component="h2">
                    {name}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    ${price}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button onClick={handleAddToCart} color="primary" size="small">
                    Add item to cart
                </Button>
                <Button onClick={handleGoToProduct} color="primary" size="small">
                    Go to product
                </Button>
            </CardActions>
        </Card>
    );
}
