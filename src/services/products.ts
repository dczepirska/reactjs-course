export interface Product {
    readonly id: string;
    readonly name: string;
    readonly shortDescription: string;
    readonly description: string;
    readonly price: number;
    readonly thumbnail?: string;
    readonly image?: string;
}

export function getProducts(page: number, elementsOnPage = 10): Promise<{ list: Product[]; totalCount: number }> {
    return Promise.resolve({
        list: [
            {
                name: 'floating',
                shortDescription: 'date island knew hello better',
                description:
                    'date island knew hello better limited dog laid farther school any science moment hurt theory boat member decide crowd increase form fifteen largest very',
                id: '9df52cd8-fcc1-5bab-b0ba-1dd31b623825',
                price: 33,
                thumbnail: 'https://cataas.com/cat?width=50&height=50',
            },
            {
                name: 'hurt',
                shortDescription: 'poetry biggest mud compass parallel whispered',
                description:
                    'poetry biggest mud compass parallel whispered more men involved plastic glass sell hung toy curious smallest remain early basic high heavy rubber child active',
                id: 'daaa6e73-cadd-59a1-8aee-b1c8f9a7219d',
                price: 4,
                thumbnail: 'https://cataas.com/cat?width=51&height=51',
            },
            {
                name: 'dark',
                shortDescription: 'thou flies now moving importance western',
                description:
                    'thou flies now moving importance western rocky satisfied roll wait torn parent soil every quietly finally father article wrapped evidence contrast tune in walk',
                id: 'cf9c0e0e-6f12-558e-9363-82c88fe7aed6',
                price: 62,
                thumbnail: 'https://cataas.com/cat?width=52&height=52',
            },
            {
                name: 'design',
                shortDescription: 'at bridge tube smell reach pay change',
                description:
                    'at bridge tube smell reach pay change boy hot upward lonely question between skin trick hurried birds fed sunlight hunter quietly clothing leg lesson',
                id: '49bfbed7-9420-51a2-a5a6-b63cbe4fd2d2',
                price: 24,
            },
            {
                name: 'hat',
                shortDescription: 'some method energy rose medicine frame careful',
                description:
                    'some method energy rose medicine frame careful given children slow nothing freedom blanket seen silence visitor twenty ever throw lose camera loud when grabbed',
                id: 'd391e8ef-9b37-5100-b5db-113c6d079159',
                price: 1,
                thumbnail: 'https://cataas.com/cat?width=54&height=54',
            },
        ],
        totalCount: 5,
    });
}
