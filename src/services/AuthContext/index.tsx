import React, { PropsWithChildren, useEffect, createContext, useState, useMemo } from 'react';
import { firebaseService } from 'services/firebase';

export type User = firebase.User | null;

interface AuthContextProps {
    readonly user?: User;
}

interface AuthContextType {
    readonly user: User;
}

export const AuthContext = createContext<AuthContextType>({
    user: null,
});

export function AuthContextProvider({ children }: PropsWithChildren<AuthContextProps>) {
    const [user, setUser] = useState<User>(null);

    useEffect(() => {
        firebaseService.getRedirectResult().then(({ user }) => setUser(user));
        firebaseService.onAuthStateChanged(setUser);
    }, []);

    const value = useMemo<AuthContextType>(() => ({ user }), [user]); //memoizacja

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
