import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: 'AIzaSyAodB5dsj4kuVLZnjii0ezTlwAU2Gcb4og',
    authDomain: 'dcz-spyro-tutorial-shop.firebaseapp.com',
    databaseURL: 'https://dcz-spyro-tutorial-shop.firebaseio.com',
    projectId: 'dcz-spyro-tutorial-shop',
    storageBucket: 'dcz-spyro-tutorial-shop.appspot.com',
    messagingSenderId: '894259740830',
    appId: '1:894259740830:web:3d655464eb3406b3febb8f',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const firebaseAuth = firebaseApp.auth();

export const firebaseService = Object.freeze({
    async signInWithGoogle() {
        await firebaseAuth.useDeviceLanguage();
        await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        return await firebaseAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    },

    async getRedirectResult() {
        return await firebaseAuth.getRedirectResult();
    },

    async signOut() {
        return await firebaseAuth.signOut();
    },

    onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseAuth),
});
